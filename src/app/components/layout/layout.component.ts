import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {

    // navbar animation

    window.addEventListener("scroll", function(){
      let header = document.getElementById('navbar11');
      header.classList.toggle("sticky-nav", window.scrollY > 120);
    });

    // search bar 

    let searchBarBtn = document.querySelector('#search-bar-btn'); 
    let searchInput = document.querySelector('.input-search');

    searchBarBtn.addEventListener("click", function(){
      searchInput.classList.toggle("search-input-none");
    });

    searchInput.addEventListener("click", function(){
      searchInput.classList.toggle("search-input-border");
    });

    // left sidebar buttons

    let leftSidebarBtn = document.querySelector('#left-sidebar-btn');
    let leftSidebar = document.querySelector('.sticky-navbar');

    leftSidebarBtn.addEventListener("click", function(){
      leftSidebar.classList.add("sticky-navbar-JS");
    });

    let leftSidebarCloseBtn = document.querySelector('#left-sidebar-close');

    leftSidebarCloseBtn.addEventListener("click", function(){
      leftSidebar.classList.remove("sticky-navbar-JS");
    });



  }

}

