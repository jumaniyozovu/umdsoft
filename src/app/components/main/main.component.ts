import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {    
    window.onscroll = function() {scroll_Function()};

    function scroll_Function() {
      if (document.body.scrollTop > 250 || document.documentElement.scrollTop > 250) {
        document.getElementById("go_up_button").className = "goupbutton",
        document.getElementById("chevronTop").className = "fal fa-chevron-up fa-2x";
      } else if (document.body.scrollTop < 250 || document.documentElement.scrollTop < 250) {
        document.getElementById("go_up_button").className = "scrolltopreverse",
        document.getElementById("chevronTop").className = "";
      }
    }
// ###################################################
    const btn = document.getElementById('go_up_button');
    btn.addEventListener('click', () => window.scrollTo({
      top: 0,
      behavior: 'smooth',
    }));
   
  }
}
