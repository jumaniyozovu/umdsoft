import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from "./components/main/main.component";
import {NotFoundPageComponent} from "./components/not-found-page/not-found-page.component";
import {LayoutComponent} from "./components/layout/layout.component";
import {ProductsComponent} from "./components/products/products.component";
import {NodejsComponent} from "./main/course/nodejs/nodejs.component";
import {PythonComponent} from "./main/course/python/python.component";
import {JavascriptFullstackComponent} from "./main/course/javascript-fullstack/javascript-fullstack.component";

const routes: Routes = [
  {path: '',component: LayoutComponent, children:[
      {path: '', component: MainComponent},
      {path: 'index', component: MainComponent},
      {path: 'course', children: [
          {path: 'nodejs-development', component: NodejsComponent},
      //    {path: 'python-development', component: PythonComponent},
        {path: 'javascript-development', component: JavascriptFullstackComponent},
        //  {path: 'android-development', component: JavascriptFullstackComponent},
        ]}
    ]},
  {path:'**', component: NotFoundPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
