import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ProductsComponent } from './components/products/products.component';
import { AdminLayoutComponent } from './components/admin/admin-layout/admin-layout.component';
import { AdminIndexComponent } from './components/admin/admin-index/admin-index.component';
import { NodejsComponent } from './main/course/nodejs/nodejs.component';
import { JavascriptFullstackComponent } from './main/course/javascript-fullstack/javascript-fullstack.component';
import { AndroidDevelopmentComponent } from './main/course/android-development/android-development.component';
import { PythonComponent } from './main/course/python/python.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    NotFoundPageComponent,
    LayoutComponent,
    ProductsComponent,
    AdminLayoutComponent,
    AdminIndexComponent,
    NodejsComponent,
    JavascriptFullstackComponent,
    AndroidDevelopmentComponent,
    PythonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
